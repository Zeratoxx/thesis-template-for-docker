MASTER  = thesis
VERSION = v0.1
NAME    = Max
SURNAME = Mustermann
OUT_DIR = build

all: check
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@echo 'First iteration of pdflatex...'
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@pdflatex --output-directory ${OUT_DIR} ${MASTER}.tex
	@echo ''
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@echo 'Biber and second iteration of pdflatex...'
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@biber --output-directory ${OUT_DIR} ${MASTER}; pdflatex --output-directory ${OUT_DIR} ${MASTER}.tex
	@echo ''
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@echo 'Third iteration of pdflatex...'
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@pdflatex --output-directory ${OUT_DIR} ${MASTER}.tex

publish: all
	@ps2pdf14 -dPDFSETTINGS=/prepress ${OUT_DIR}/${MASTER}.pdf
	@mv ${MASTER}.pdf.pdf ${OUT_DIR}/${MASTER}.pdf

check: clean
	@chktex ${MASTER}.tex

clean:
	@test -d "${OUT_DIR}" && cd ${OUT_DIR} || mkdir ${OUT_DIR}
	@rm -rf *run.xml *-blx.bib *.aux *.bbl *.blg *.brf *.log *.lof *.lot *.lol *.out *.tcp *.toc *.tps *.bak *.backup *.pdfsync *.synctex.gz *.*~
	@for i in run.xml -blx.bib aux bbl blg brf log lof lot lol out tcp toc tps bak backup pdfsync synctex.gz; do find . -name *.$$i -exec rm -f {} + ; done
	@find . -name *.*~ -exec rm -f {} +
	@cd ..

cleanall: clean
	@rm -rf *.pdf ${OUT_DIR}

test: check
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@echo 'First iteration of pdflatex...'
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@pdflatex --interaction=nonstopmode --halt-on-error --output-directory ${OUT_DIR} ${MASTER}.tex
	@echo ''
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@echo 'Biber and second iteration of pdflatex...'
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@biber --output-directory ${OUT_DIR} ${MASTER}; pdflatex --output-directory ${OUT_DIR} ${MASTER}.tex
	@echo ''
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@echo 'Third iteration of pdflatex...'
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
	@pdflatex --output-directory ${OUT_DIR} ${MASTER}.tex

bz2: clean
	@echo 'creating package including Docs'
	@tar --exclude-vcs -cf `pwd`/../${MASTER}-${NAME}_${SURNAME}-${VERSION}-`date +%Y%m%d`.tar `pwd`/../`pwd | sed "s,^\(.*/\)\?\([^/]*\),\2,"`
	@bzip2 `pwd`/../${MASTER}-${NAME}_${SURNAME}-${VERSION}-`date +%Y%m%d`.tar

bz2-small: clean
	@echo 'creating package excluding Docs'
	@tar --exclude-vcs --exclude=Docs -cf `pwd`/../${MASTER}-${NAME}_${SURNAME}-${VERSION}-`date +%Y%m%d`_small.tar `pwd`/../`pwd | sed "s,^\(.*/\)\?\([^/]*\),\2,"`
	@bzip2 `pwd`/../${MASTER}-${NAME}_${SURNAME}-${VERSION}-`date +%Y%m%d`_small.tar
